﻿using System;

namespace QuadraticEquation
{
    public class QuadraticEquation
    {   
        public double[] solve(double a, double b, double c)
        {
            double d2 = b*b - 4*a*c;
            
            if (Math.Abs(a) < 1e-5)
            {
                throw new ArithmeticException("coefficient a cannot be equal to 0");
            }

            if (d2 >= 0)
            {
                double d = Math.Sqrt(d2);

                return new double[] {(-b+d)/(2*a), (-b-d)/(2*a)};
            }

            return new double[0];
        }
    }
}
