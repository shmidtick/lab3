using System;
using Xunit;

namespace QuadraticEquation.tests
{
    public class UnitTest1
    {
        [Fact]
        public void SquareEquation_1_Minus2_1_MustHave_TwoSameRoots()
        {
            QuadraticEquation Equation = new QuadraticEquation();

            double[] result = Equation.solve(1, -2, 1);

            Assert.True(Equal(result[1], 1));
            Assert.True(Equal(result[0], 1));
        }

        [Fact]
        public void SquareEquation_1_0_Minus1_MustHave_TwoRoots()
        {
            QuadraticEquation Equation = new QuadraticEquation();

            double[] result = Equation.solve(1, 0, -1);
            Array.Sort(result);
            Assert.True(Equal(result[0], -1));
            Assert.True(Equal(result[1], 1));
        }

        [Fact]
        public void SquareEquation_1_0_1_DoesntHaveRoots()
        {
            QuadraticEquation Equation = new QuadraticEquation();

            double[] result = Equation.solve(1, 0, 1);

            Assert.Equal(result, new double[0]);
        }

        [Fact]
        public void coefficient_a_CannotBeEqual_to_0()
        {
            QuadraticEquation Equation = new QuadraticEquation();
            
            Exception exc = Assert.Throws<ArithmeticException>(()=>Equation.solve(0, 1, 1));    

            Assert.Equal("coefficient a cannot be equal to 0", exc.Message);
        }

        private bool Equal(double a, double b, double eps = 1e-5)
        {
            return Math.Abs(a - b) < eps;
        }
    }
}
